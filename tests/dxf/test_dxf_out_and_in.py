# -*- coding: UTF-8 -*-
""" AutoCAD 遠隔操作用ライブラリ
Library for remote control of Autocad Japanese Edition

The MIT License (MIT)

Copyright (c) 2017 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from __future__ import print_function
import os
from acadremoteoperator import AcadRemoteOperator
import ezdxf


def __move_points(points):
    """ X座標を50.0mm移動する """
    x, y, z = points
    x += 50.0
    return x, y, z


def edit_dxf(dxffile):
    """ DXFファイルを編集する """
    # encoding='cp932'を付けると文字化けする。
    dwg = ezdxf.readfile(dxffile)

    modelspace = dwg.modelspace()

    for e in modelspace:
        if e.dxftype() == 'LINE':
            # 始点の座標を移動する
            e.set_dxf_attrib('start', (__move_points(e.get_dxf_attrib('start'))))
            # 終点の座標を移動する
            e.set_dxf_attrib('end', (__move_points(e.get_dxf_attrib('end'))))

    dwg.save()


if __name__ == '__main__':
    dxffile = os.path.join(os.path.abspath(os.path.dirname(__file__)), u'dxf出力のテスト.dxf')
    cadapp = AcadRemoteOperator()

    # cadapp.dxf_out(dxffile)
    cadapp.dxf_out(dxffile, status_string=u'【線分を選択してください】')

    edit_dxf(dxffile)

    # UNDO 記録開始
    cadapp.send_command('UNDO{ENTER}BE{ENTER}')

    # 直前の選択を削除する
    cadapp.send_command('ERASE{ENTER}P{ENTER 2}')

    cadapp.dxf_in(dxffile)

    # UNDO 記録終了
    cadapp.send_command('UNDO{ENTER}E{ENTER}')
