# -*- coding: UTF-8 -*-
""" AutoCAD 遠隔操作用ライブラリ
Library for remote control of Autocad Japanese Edition

The MIT License (MIT)

Copyright (c) 2017 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from __future__ import print_function

import os
import ezdxf


def count_ent(ent_type):
    """ エンティティ数を数える """
    global counter
    if ent_type not in counter:
        counter[ent_type] = 0
    counter[ent_type] += 1
    print('*** {0} No. {1}:'.format(ent_type, counter[ent_type]))


def disp_attr(e):
    """ DXF アトリビュートを表示する """
    # [print(item) for item in dir(e)]
    for attr in e.DXFATTRIBS.keys():
        try:
            print('{0}: {1}'.format(attr, e.get_dxf_attrib(attr)))
        except ValueError:
            print('{0}: ValueError'.format(attr))


dxffile = os.path.join(os.path.abspath(os.path.dirname(__file__)), u'text2.dxf')
# dxffile = os.path.join(os.path.abspath(os.path.dirname(__file__)), u'dxf出力のテスト.dxf')
dwg = ezdxf.readfile(dxffile)

modelspace = dwg.modelspace()

counter = {}
for e in modelspace:
    count_ent(e.dxftype())
    disp_attr(e)

print(counter)
dwg.save()
