#!python
# -*- coding: UTF-8 -*-
""" AutoCAD 遠隔操作用ライブラリ
Library for remote control of Autocad Japanese Edition

The MIT License (MIT)

Copyright (c) 2017 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
# セットアップ用ファイル

from distutils.core import setup

setup(
    name='acadremoteoperator',
    version='0.0.0.2017.403',
    description='Library for remote control of AutoCAD Japanese Edition',
    author='極楽鳥(bird_of_paradise)',
    author_email='',
    url='https://bitbucket.org/birds_of_paradise_2015/acadremoteoperator/',
    packages=[
        'acadremoteoperator',
        # 'acadremoteoperator.docs',
        'acadremoteoperator.tests',
        'acadremoteoperator.tests.dxf',
    ],
    package_dir={
        'acadremoteoperator': '',
        # 'docs': 'acadremoteoperator.docs',
        'tests': 'acadremoteoperator.tests',
        'tests/dxf': 'acadremoteoperator.tests.dxf',
    },
    package_data={
        'acadremoteoperator': ['tests/dxf/*.dxf', 'tests/dxf/*.dwg'],
    },
    license='The MIT License',
    requires=['pywinauto', 'ezdxf'],
    classifiers=[
        'Development Status :: 1 - Planning',
        'Environment :: Console',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: Japanese',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: Implementation :: CPython',
    ],
)
