# -*- coding: UTF-8 -*-
""" AutoCAD 遠隔操作用ライブラリ
Library for remote control of Autocad Japanese Edition

The MIT License (MIT)

Copyright (c) 2017 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
# TODO: もっと気の利いたライブラリ名とクラス名にする
# TODO: Type Hint を使用する

from __future__ import print_function

import os
import re
import win32api
import win32con

from pywinauto.application import Application
from pywinauto import findwindows
from pywinauto import clipboard
from pywinauto import timings

# 取得するメインウィンドウのタイトル（正規表現で表現する）
MAIN_WINDOW_TITLE = u'Autodesk AutoCAD'
# 取得するテキストウィンドウのタイトル（正規表現で表現する）
TEXT_WINDOW_TITLE = u'AutoCAD テキスト ウィンドウ'

COMMAND_PROMPT = u'コマンド: '
SELECT_OBJECT_PROMPT = u'オブジェクトを選択: '

BITMASK = 0x8000  # win32api.GetAsyncKeyState()で使用


class MainWindowNotFoundError(Exception):
    """メインウィンドウが発見できなかった場合の例外"""
    pass


class TextWindowNotFoundError(Exception):
    """テキストウィンドウが発見できなかった場合の例外"""
    pass


class UserPressEscapeError(Exception):
    """ユーザーのエスケープキー入力による例外"""
    pass


class IncorrectVariableNameError(Exception):
    """ユーザーの入力した変数名が間違っていることによる例外"""
    pass


class AcadRemoteOperator(object):
    """ AutoCAD 遠隔操作用基本クラス """

    def __init__(self):
        """ 初期化 """
        super().__init__()

        # TODO: Application.start()に対応
        self.cad = Application().connect(title_re=MAIN_WINDOW_TITLE)

        try:
            # pywinauto.application.WindowSpecification object
            self.main_window_w = self.cad.top_window()
            # pywinauto.controls.win32_controls.DialogWrapper object
            self.main_window = self.main_window_w.wrapper_object()
        except findwindows.ElementNotFoundError:
            raise MainWindowNotFoundError

        self.text_window_w = self.cad.window(title_re=TEXT_WINDOW_TITLE)
        try:
            findwindows.find_element(title_re=TEXT_WINDOW_TITLE)
        except findwindows.ElementNotFoundError:
            try:
                self.main_window_w.type_keys('{F2}')
            except findwindows.ElementNotFoundError:
                raise TextWindowNotFoundError

        self.text_window_w.wait('exists enabled visible ready')

        # pywinauto.controls.win32_controls.DialogWrapper object
        self.text_window = self.text_window_w.wrapper_object()

        self.cmd_line_w = self.text_window_w.window(
            top_level_only=False, control_id=2)
        # self.cmd_line_w = self.text_window_w.window(title_re=COMMAND_PROMPT, top_level_only=False, control_id=2)
        # pywinauto.controls.hwndwrapper.HwndWrapper object
        self.cmd_line = self.cmd_line_w.wrapper_object()

    def show_text_window(self) -> None:
        """ テキスト ウィンドウの表示 """
        try:
            findwindows.find_element(
                handle=self.text_window.element_info.handle)
        except findwindows.ElementNotFoundError:
            try:
                self.main_window_w.type_keys('{F2}')
                self.main_window_w.wait('exists enabled visible ready')
            except findwindows.ElementNotFoundError:
                raise TextWindowNotFoundError

    def hide_text_window(self) -> None:
        """ テキスト ウィンドウの非表示 """
        # FIXME: hide_text_window()実行後show_text_window()すると例外になる？
        # テキスト ウィンドウを最小化するほうが無難？
        # 廃止するか？
        if self.text_window_w.exists():
            self.text_window_w.type_keys('{VK_MENU}{SPACE}H')

    def get_history(self) -> tuple:
        """ ヒストリの取得

        :return: 「ヒストリの取得」で取得した文字列
        :rtype: tuple
        """
        self.show_text_window()
        self.text_window_w.type_keys('{VK_MENU}EH')
        self.text_window_w.wait('exists enabled visible ready')
        return tuple(clipboard.GetData().split('\r\n'))

    def prompt_state(self) -> str:
        """ 現在のプロンプトの状態取得

        :return: 現在のプロンプトに表示されている文字列
        :rtype: str
        """
        self.show_text_window()
        return self.cmd_line.window_text()

    def prompt_is_ready(self) -> bool:
        """ コマンド入力待ち状況の確認

            現在のプロンプトが「コマンド: 」になっているかどうか？

        :return: コマンド入力待ち状況ならTrue。でなければFalse。
        :rtype: bool
        """
        return bool(self.prompt_state() == COMMAND_PROMPT)

    # TODO: 妥当な待ち時間を見つけ出す。
    @timings.always_wait_until(2, 0.5)
    def wait_prompt_state(self, prompt_string_re: str) -> bool:
        """ 現在のプロンプトの状態が希望の文字列かどうかの確認

        :param prompt_string_re: 希望するプロンプトの状態を正規表現で表現
        :type prompt_string_re: str
        :return: 希望するプロンプトならTrue。でなければFalse。
                 Falseの場合、@timings.always_wait_untilメソッドにより、
                 pywinauto.timings.TimeoutErrorが発生する。
        :rtype: bool
        """
        return_value = re.search(prompt_string_re, self.prompt_state())
        return bool(return_value is not None)

    def can_select(self) -> bool:
        """ 現在のプロンプトがオブジェクトの選択待ちかどうかの確認

            現在のプロンプトが「オブジェクトを選択: 」になっているかどうか？

        :return: 「オブジェクトを選択: 」待ち状況ならTrue。でなければFalse。
        :rtype: bool
        """
        return bool(self.prompt_state() == SELECT_OBJECT_PROMPT)

    def send_command(self, command: str) -> None:
        """ コマンド文字列の送信

            AutoCADに対してコマンド文字列を送信する。
            送信するキーは以下のWEBページによる
            [pywinauto\.keyboard](https://pywinauto.readthedocs.io/en/latest/code/pywinauto.keyboard.html)
        :param command: 送信するコマンド文字列
        :type command: str
        """
        # FIXME: 上手く文字列を送信できない場合がある不具合の修正
        self.show_text_window()
        # self.text_window_w.wait('exists enabled visible ready')
        self.cmd_line.get_active()
        self.cmd_line.set_focus()
        self.cmd_line.send_chars(command)
        # self.text_window_w.wait('exists enabled visible ready')

    def __wait_key_press(self) -> None:
        """ キーボード・マウスからの入力待ち """
        while True:
            if win32api.GetAsyncKeyState(win32con.VK_RETURN) & BITMASK:  # エンターキー
                # print('you press enter.')
                break
            elif win32api.GetAsyncKeyState(win32con.VK_RBUTTON) & BITMASK:  # マウス右クリック
                # print('you press rbutton.')
                break
            elif win32api.GetAsyncKeyState(win32con.VK_ESCAPE) & BITMASK:  # エスケープキー
                # print('you press escape.')
                raise UserPressEscapeError

    def select_objects(self, command='') -> None:
        """ オブジェクトの選択

        :param command: AutoCADに送信するコマンド文字列。
                        オブジェクトの選択関係。
        :type command: str
        """
        if not self.can_select():
            self.send_command('{VK_ESCAPE 2}')

        self.text_window.minimize()
        self.main_window.get_active()
        self.main_window.set_focus()

        if command == '':
            self.send_command('SELECT{ENTER}')
            self.__wait_key_press()
        else:
            # TODO: コードの検証が必要（細かい動作は決めてない）
            self.send_command(command)

        self.text_window.restore()
        self.text_window_w.wait('exists enabled visible ready')

        if self.can_select():
            self.send_command('{ENTER}')

    def dxf_out(self, dxffile: str, status_string='', select_string='', version=2013, prec=16) -> None:
        """ DXFへ図形出力

        「_DXFOUT」を用いてDXFファイルに図形を出力します。

        :param dxffile: DXFファイルの絶対パス
        :type dxffile: str
        :param status_string: ステータスバーに表示する文字列
        :type status_string: str
        :param select_string: AutoCADに送信するコマンド文字列。
                              オブジェクトの選択関係。
        :type select_string: str
        :param version: DXFのバージョン
        :type version: int
        :param prec: 精度
        :type prec: int
        """
        self.text_window_w.wait('exists enabled visible ready')

        self.wait_prompt_state(COMMAND_PROMPT)
        old_filedia = self.get_variable('FILEDIA')
        self.wait_prompt_state(COMMAND_PROMPT)
        self.set_variable('FILEDIA', '0')

        if status_string != '':
            self.wait_prompt_state(COMMAND_PROMPT)
            self.set_variable('MODEMACRO', status_string)

        self.select_objects(select_string)

        self.wait_prompt_state(COMMAND_PROMPT)
        self.send_command('_DXFOUT{ENTER}')

        self.wait_prompt_state('図面に名前を付けて保存')
        self.send_command(dxffile + '{ENTER}')

        self.wait_prompt_state('精度として小数点以下の桁数')
        self.send_command('O{ENTER}')
        self.send_command('P{ENTER 2}')

        self.wait_prompt_state('精度として小数点以下の桁数')
        self.send_command('V{ENTER}')
        self.send_command(str(version) + '{ENTER}')

        self.wait_prompt_state('精度として小数点以下の桁数')
        self.send_command(str(prec) + '{ENTER}')

        try:
            self.wait_prompt_state('置き換えますか')
            self.send_command('Y')
            self.send_command('{ENTER}')
        except timings.TimeoutError:
            pass

        # self.text_window_w.wait('exists enabled visible ready')

        self.wait_prompt_state(COMMAND_PROMPT)
        self.set_variable('FILEDIA', old_filedia)

        if status_string != '':
            self.wait_prompt_state(COMMAND_PROMPT)
            self.set_variable('MODEMACRO', '.')

    def dxf_in(self, dxffile: str, explode=True) -> None:
        """ DXFから図形挿入

        :param dxffile: DXFファイルの絶対パス
        :type dxffile: str
        :param explode: 挿入した図形を分解するかどうか
                        そのままの状態だとブロックのままなので
        :type explode: bool
        """
        # TODO: 引数で指示するようにする。
        # 実際はブロックの挿入です。
        self.wait_prompt_state(COMMAND_PROMPT)
        self.send_command('-INSERT{ENTER}')

        self.wait_prompt_state('ブロック名を入力')
        self.send_command(dxffile + '{ENTER}')

        self.wait_prompt_state('挿入位置を指定')
        self.send_command('0,0{ENTER}')

        self.wait_prompt_state('X 方向の尺度を入力')
        self.send_command('1{ENTER}')

        self.wait_prompt_state('Y 方向の尺度を入力')
        self.send_command('1{ENTER}')

        self.wait_prompt_state('回転角度を指定')
        self.send_command('0{ENTER}')

        # ブロックを分解する
        if explode:
            self.wait_prompt_state(COMMAND_PROMPT)
            self.send_command('_EXPLODE{ENTER}L{ENTER 2}')

            # 未使用になったブロックの名前削除
            block_name, _ = os.path.splitext(os.path.basename(dxffile))
            self.wait_prompt_state(COMMAND_PROMPT)
            self.send_command('-PURGE{ENTER}')
            self.wait_prompt_state('名前削除する未使用のオブジェクトのタイプを入力')
            self.send_command('B{ENTER}')
            self.wait_prompt_state('削除する名前を入力')
            self.send_command(block_name + '{ENTER}')
            self.wait_prompt_state('名前削除されるそれぞれの名前を確認しますか')
            self.send_command('N{ENTER}')

    def __check_variable(self) -> None:
        """ 入力した変数名が正しかったのかの事後チェック """
        if re.search('(そのようなコマンド.+はありません。|そのような変数名はありません。)',
                     self.get_history()[-2]) is not None:
            raise IncorrectVariableNameError('変数名の入力間違いです。')

    def get_variable(self, variable: str) -> str:
        """ 変数の値取得

        :param variable: AutoCADのシステム変数／環境変数の名前
        :type variable: str
        :return: 現在のAutoCADのシステム変数／環境変数の値
        :rtype: str
        """
        self.wait_prompt_state(COMMAND_PROMPT)
        self.send_command(variable + '{ENTER}')

        try:
            self.wait_prompt_state('の新しい値を入力')
            return_value = re.search(
                'の新しい値を入力 \<\"?(?P<param>.*)\"?\>\:', self.prompt_state())
        except timings.TimeoutError:
            self.__check_variable()
            return_value = re.search(
                ' = \"(?P<param>.*)\"', self.get_history()[-1])

        self.text_window_w.wait('exists enabled visible ready')

        if not self.prompt_is_ready():
            self.send_command('{ESC}')

        return return_value.group('param')

    def set_variable(self, variable_name: str, new_variable: str) -> None:
        """ 変数への値設定

        :param variable_name: AutoCADのシステム変数／環境変数の名前
        :type variable_name: str
        :param new_variable: 新しいAutoCADのシステム変数／環境変数の値
        :type new_variable: str
        """
        self.wait_prompt_state(COMMAND_PROMPT)
        self.send_command('_SETVAR{ENTER}')

        self.wait_prompt_state('変数名を入力')
        self.send_command(variable_name + '{ENTER}')

        try:
            self.wait_prompt_state('の新しい値を入力')
            self.send_command(new_variable + '{ENTER}')
        except timings.TimeoutError:
            self.__check_variable()

        self.text_window_w.wait('exists enabled visible ready')

    def pick_points(self) -> tuple:
        """ 点を指示

        :return: 指示したxyz座標
        :rtype: tuple(str, str, str)
        """
        self.send_command('_ID{ENTER}')
        self.wait_prompt_state('点を指定: ')

        self.text_window.minimize()

        # 「コマンド: 」が表示されるまで待機
        while True:
            if win32api.GetAsyncKeyState(win32con.VK_ESCAPE) & BITMASK:  # エスケープキー
                raise UserPressEscapeError
            try:
                self.wait_prompt_state(COMMAND_PROMPT)
                break
            except timings.TimeoutError:
                pass

        self.text_window.restore()

        # print(self.get_history()[-2])
        return_value = re.search('点を指定:  X = (?P<x>[\-\.0-9]+) +Y = (?P<y>[\-\.0-9]+) +Z = (?P<z>[\-\.0-9]+)',
                                 self.get_history()[-2])
        if return_value is None:
            raise Exception('正規表現にエラーがあります。座標を取得できませんでした。')

        return return_value.group('x'), return_value.group('y'), return_value.group('z')


if __name__ == '__main__':
    pass
