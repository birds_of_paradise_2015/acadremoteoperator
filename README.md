# AcadRemoteOperator

Library for remote control of AutoCAD Japanese Edition  

AutoCAD 遠隔操作用Pythonライブラリ（試作）

-----

## 前提

- 本ライブラリを利用したスクリプトを実行するには、予めAutoCADが起動していること。

- 「AutoCAD テキスト ウィンドウ」が表示可能になっていること。

- コマンドプロンプトで作業します。

## 注意

- 本ライブラリは**動作が不安定**です。

    上手く動く場合もあれば、動かない場合もあったりします。
    
- 本ライブラリはせっかちな人にはまるで向きません。

## インストール

以前に本ライブラリをインストールしている場合は、

`pip uninstall acadremoteoperator`

でアンインストールした後に、

`python setup.py install`

でインストールしてください。

## 仕様

仕様は未確定のため今後**変更する場合があります**。その旨、予めご了承ください。

## リファレンス

準備できていません。直接コードを確認してください。

## DXF/DWGファイルの読み書き

本ライブラリは直接的にはDXFファイルを取り扱いません。  

ただし、testsファルダ内にてezdxfを用いてDXFファイルを読み書きするスクリプトがあります。  

DWGファイルの読み書きについては用意していません。

## 依存ライブラリ

本ライブラリは以下のライブラリに依存しています。  
予めインストールしてください。

- pywinauto
- ezdxf

    DXFファイルの読み書きに必要です。

## 動作確認環境

- AutoCAD 2016 (64 bit)
- Python 3.6.0 (64 bit)
- pywinauto 0.6.2
- ezdxf 0.7.9

## リンク

- [Python](https://www.python.org/)
- [pywinauto](http://pywinauto.github.io/)
- [ezdxf](https://bitbucket.org/mozman/ezdxf)

## ライセンス

[the MIT License](http://opensource.org/licenses/mit-license.php)
で公開しています。

## 履歴

- 2017/04/06 公開
